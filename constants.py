KERAS_MODEL_URL = 'http://keras:5000/predict'
LOCAL_KERAS_MODEL_URL = 'http://localhost:5000/predict'

KERAS_LABEL = 'Aeron'
KERAS_PROBABILITY_THRESHOLD = 0.9

CRAIGSLIST_PRICE_THRESHOLD = 350

REDIS_IMAGE_QUEUE_KEY = 'queue:craigslist-aeron-images'
REDIS_IMAGE_URL_LIST_KEY = 'set:craigslist-aeron-urls'