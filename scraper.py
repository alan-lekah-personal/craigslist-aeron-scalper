import logging as log
from craigslist import CraigslistForSale
import constants
import requests
import platform
import json
import os
import glob
import redis
import sys

log.basicConfig(level=log.DEBUG)

if __name__ == '__main__':
    # go fetch office chairs in the general area
    cl_s = CraigslistForSale(site="miami", category="sss", filters={"query": "office chair",
                                                                    "max_price": constants.CRAIGSLIST_PRICE_THRESHOLD,
                                                                    "has_image": True,
                                                                    "posted_today": True,
                                                                    "search_distance": 50,
                                                                    "zip_code": 33446
                                                      })

    r = redis.Redis(
        host='redis' if platform.system() != 'Darwin' else 'localhost',
        port='6379',
        password='FNZjXUlC1Bu07WA7mBCS')

    ping = r.ping()
    if not ping:
        log.error("Cannot connect to redis!")
        sys.exit(1)

    # mypath = '/Users/alekah/Downloads/image-classification-keras/machinelearning/images/*/*'
    # onlyfiles = [f.rsplit('/', 1)[-1] for f in glob.glob(mypath)]

    # sort through the newest ones
    for result in cl_s.get_results(sort_by='newest', limit=100, include_details=True):

        # skip results without pictures
        if 'images' not in result:
            continue

        # grab the first couple images and send to the model
        for image in result['images']:
            # grab the image
            image_request_response = requests.get(image)

            # send to the keras model
            keras_model_response = requests.post(constants.KERAS_MODEL_URL
                                                 if platform.system() != 'Darwin'
                                                 else constants.LOCAL_KERAS_MODEL_URL,
                                                 files={'image': image_request_response.content},
                                                 timeout=30)

            keras_model_json_response = json.loads(keras_model_response.content)
            log.info(keras_model_json_response)

            # successfully figured out our image
            if keras_model_json_response and keras_model_json_response['success'] is True \
                    and 'predictions' in keras_model_json_response:
                # find the correct keras label
                for dic in keras_model_json_response['predictions']:
                    # if the probability meets our threshold
                    # send a message
                    if dic['label'] == constants.KERAS_LABEL \
                            and dic['probability'] > constants.KERAS_PROBABILITY_THRESHOLD:

                        image_url_exists_in_redis = r.exists(constants.REDIS_IMAGE_URL_LIST_KEY, image)
                        # check if the image URL already exists in the set
                        if not image_url_exists_in_redis:
                            log.info(f"Found a unicorn {dic['probability']} - {result['url']}")

                            redis_image_push_result = r.rpush(constants.REDIS_IMAGE_QUEUE_KEY, image)
                            if not redis_image_push_result:
                                log.error(f"Error pushing image to redis! - {image}")
                            else:
                                redis_url_set_add_result = r.sadd(constants.REDIS_IMAGE_URL_LIST_KEY, image)
                                if not redis_url_set_add_result:
                                    log.error(f"Error pushing image URL to redis! - {image}")
                        else:
                            log.debug("Image already exists in redis!")

                        # image_name = image.rsplit('/', 1)[-1]
                        # if image_name not in onlyfiles:
                        #     with open(os.path.join('matched_images', image_name), 'wb') as f:
                        #         f.write(image_request_response.content)
